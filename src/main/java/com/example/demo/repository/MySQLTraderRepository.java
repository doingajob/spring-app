package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Trader;

@Repository
public class MySQLTraderRepository implements TraderRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public Trader getTraderById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT id, stockTicker, price, volume, buyOrSell, statusCode FROM Details WHERE id=?";
		return template.queryForObject(sql, new TraderRowMapper(), id);
	}

	@Override
	public Trader editTrader(Trader trader) {
		// TODO Auto-generated method stub
		String sql = "UPDATE Details SET stockTicker = ?, price = ? , volume = ? , buyOrSell = ? , statusCode = ?"
				+ "WHERE id = ?";
		template.update(sql, trader.getStockTicker(), trader.getPrice(), trader.getVolume(), trader.getBuyOrSell(),
				trader.getStatusCode());
		return trader;
	}

	@Override
	public int deleteTrader(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM Details WHERE id = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Trader addTrader(Trader trader) {
		// TODO Auto-generated method stub
		String sql = "Insert into Details(stockTicker, price, volume, buyOrSell, statusCode) "
				+ "VALUES(?, ?, ?, ?, ?)";
		template.update(sql, trader.getStockTicker(), trader.getPrice(), trader.getVolume(), trader.getBuyOrSell(),
				trader.getStatusCode());
		return trader;
	}

	@Override
	public List<Trader> getAllTraders() {
		String sql = "SELECT * FROM Details";
		return template.query(sql, new TraderRowMapper());
	}

}

class TraderRowMapper implements RowMapper<Trader> {

	@Override
	public Trader mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Trader(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),rs.getString("buyOrSell"), rs.getInt("statusCode"));
	}
}
