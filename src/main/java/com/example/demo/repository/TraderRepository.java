package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Trader;

@Component
public interface TraderRepository {

	public List<Trader> getAllTraders();

	public Trader getTraderById(int id);

	public Trader editTrader(Trader trader);

	public int deleteTrader(int id);

	public Trader addTrader(Trader trader);
}
