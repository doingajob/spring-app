package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//Add service
import com.example.demo.entities.Trader;
import com.example.demo.service.TraderService;


@RestController
@RequestMapping("api/traders")
public class TradingController {
	@Autowired
	TraderService service;
	
	@GetMapping(value = "/")
	public List<Trader> getAllTraders() {
		return service.getAllTraders();
	}
	
	@GetMapping(value = "/{id}")
	public Trader getTraderById(@PathVariable("id") int id) {
	  return service.getTrader(id);
	}

	@PostMapping(value = "/")
	public Trader addTrader(@RequestBody Trader trader) {
		return service.newTrader(trader);
	}

	@PutMapping(value = "/")
	public Trader editTrader(@RequestBody Trader trader) {
		return service.saveTrader(trader);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteTrader(@PathVariable int id) {
		return service.deleteTrader(id);
	}

}

