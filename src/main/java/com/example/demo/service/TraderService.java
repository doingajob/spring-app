package com.example.demo.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entities.Trader;
import com.example.demo.repository.TraderRepository;

//Comment
@Service
public class TraderService {
	@Autowired
	private TraderRepository repository;
	public List<Trader> getAllTraders() {
		return repository.getAllTraders();}
		public Trader getTrader(int id) {
			return repository.getTraderById(id);
		}

		public Trader saveTrader(Trader trader) {
			return repository.editTrader(trader);
		}

		public Trader newTrader(Trader trader) {
			return repository.addTrader(trader);
		}

		public int deleteTrader(int id) {
			return repository.deleteTrader(id);
		}

}
